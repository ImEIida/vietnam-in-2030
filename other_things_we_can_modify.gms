********************************************************************************
$ontext

   GTAP in GAMS project

   GAMS file : Other changes

   @purpose  : Adjustments in the demand, output, transportation, import data
   @author   : W. Britz, using code by Tom Rutherford
   @date     : 24.10.13
   @since    :
   @refDoc   :
   @seeAlso  :
   @calledBy :

$offtext
********************************************************************************

*change in total consumption in region r
	c(r) = c(r)*1.XX; 
	vpm(r) = vpm(r)*1.XX; 
	pc(r) = pc(r)*1.XX; 
*change in public consumption: -> I'm not sure whether we can do this. 
	g(r) = g(r)*1.XX; 
*change in firm output: 
	y(i,r) = y(i,r)*1.XX;
	vom(i,r) = vom(i,r)*1.XX; 
*change in imports: 
	m(i,r) = m(i,r)*1.XX;
	vim(i,r) = vim(i,r)*1.XX;  
*change in transportation services: 
	yt(j) = yt(j)*1.XX; 
	vtw(j) = vtw(j)*1.XX; 
*change in primary factor endowments: 
	evom(f,r) = evom(f,r)*1.XX; 
*change for specific factors: 
*   
	ps(sf,j,r) = ps(sf,j,r)*1.XX; 
* 
	pf(sf,j,r) = pf(sf,j,r)*1.XX; 
*
	vfm(sf,j,r) = vfm(sf,j,r)*1.XX; 

* 
* ---- We can also impose all these changes on the demand side
*



* see all market clearance conditions below: 
*        -----------------------------------------------------------------------------
*
*        Market clearance associated with private consumption:

*
*          c(r)    consumption index
*          vpm(r)  private consumption in base
*          pc(r)   consumer price index
*
mkt_pc(r)..                c(r) * vpm(r) * pc(r) =e= ra(r);

*        -----------------------------------------------------------------------------
*        Market clearance associated with public consumption:

mkt_pg(r)..                g(r) =e= 1;

*        -----------------------------------------------------------------------------
*        Market clearance associated with firm output:

mkt_py(i,r) $ vom(i,r) ..

         y(i,r) * vom(i,r) =e=
*
*                               --- intermediate demand for domestic sales
*
                                   sum(j $ vdfm(i,j,r), ddfm(i,j,r))
*
*                               --- household demand for domestic sales
*
                                 + ddpm(i,r) $ vdpm(i,r)
*
*                               --- government demand for domestic sales
*
                                 + ddgm(i,r) $ vdgm(i,r)
*
*                               --- export demand
*
                                 + sum(s $ vxmd(i,r,s), dxmd(i,r,s))
*
*                               --- demand for international transportation
*
                                 + dst(i,r) $ vst(i,r)
*
*                               --- investment demand
*
                                 + vdim(i,r);

*
*        -----------------------------------------------------------------------------
*
*        Market clearance associated with imports:
*
*        m(i,r)    import quantity index
*        vim(i,r)  import quantity in base
*
*
*mkt_pm(i,r) $ vim(i,r)..     m(i,r) * vim(i,r) =e=
*
*                                                      --- intermediate demand for imports
*
*                                                         sum(j $ vifm(i,j,r), difm(i,j,r))
*
*                                                      --- private demand for imports
*
*                                                      + dipm(i,r) $ vipm(i,r)
*
*                                                      --- government demand for imports
*
*                                                       + digm(i,r) $ vigm(i,r);
*
*        -----------------------------------------------------------------------------
*
*        Market clearance associated with transport services:
*
*mkt_pt(j) $ vtw(j)..         yt(j) * vtw(j) =e= sum((i,s,r) $ vtwr(j,i,s,r), dtwr(j,i,s,r));
*
*
*
*        -----------------------------------------------------------------------------
*
*        Market clearance associated with primary factors:
*
*mkt_pf(f,r) $ (evom(f,r) $ (not fpf(f))) ..    evom(f,r) =e= sum(j $ vfm(f,j,r), dfm(f,j,r)) $ mf(f) + (evom(f,r)*ft*(f,r))$sf(f);
*
*ft.l(sf,r) = 1;
*
*        -----------------------------------------------------------------------------
*
*        Market clearance associated with specific factors:
*
*mkt_ps(sf,j,r) $ vfm(sf,j,r)..
*
*         vfm(sf,j,r) * (ps(sf,j,r)/pf(sf,r))**etrae(sf) =e= dfm(sf,j,r);
*
*


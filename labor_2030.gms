********************************************************************************
$ontext

   GTAP in GAMS project

   GAMS file : all 2030

   @purpose  : Expected changes in labor, capital, total factor productivity by 2030 - except land endowment
   @author   : 
   @date     : 05.12.15
   @since    :
   @refDoc   :
   @seeAlso  :
   @calledBy :

$offtext
********************************************************************************
*
*** HERE:Data taken from data file in the Dropbox
*
*LABOR
* --- skilled labor
   evom("SkLab","VietNam") = evom("Sklab","VietNam")*1.6;
   evom("SkLab","SEA") = evom("sklab","SEA")*1.518;
   evom("SkLab","RestofWorld") = evom("sklab","RestofWorld")*.972;
* --- unskilled labor
   evom("UnSkLab","VietNam") = evom("UnSkLab","VietNam")*.86;
   evom("UnSkLab","SEA") = evom("UnSkLab","SEA")*1.71;
   evom("UnSkLab","RestofWorld") = evom("UnSkLab","RestofWorld")*1.59;







* --- explanation: 
* By 2030 we expect population growth by XX percent in VietNam, SEA and RoW. 
* We assume the relative growth of skilled labor to be the same/higher/lower than
* growth of unskilled labor in VietNam, SEA, RoW
*
*
* --- annotations: 
* When we change evom(f,r) the value shares will change too. We must check if these 
* new value shares are plausible in the end. See value share definitions below.
*
*
*
* --- share for each factor f on factor cost (vfm) plus factor taxes (rtf)
*
*  thetaf(f,j,r) $ sum(ff,vfm(ff,j,r)*(1+rtf0(ff,j,r)))
*         = vfm(f,j,r) * (1+rtf0(f,j,r)) / sum(ff, vfm(ff,j,r) * (1+rtf0(ff,j,r)) );
*
*
* --- share of domestic intermediate demand cost for product i by sector j
*     on total intermediate demand cost for product i by sector j
*     vdfm=value of domestic firm demand, rftf0=related ad-valorem tax rate, benchmark
*     vifm=value of imported firm demand, rfti0=related ad-valorem tax rate, benchmark
*
*  thetad(i,j,r) $ (vdfm(i,j,r)*(1+rtfd0(i,j,r)) + vifm(i,j,r)*(1+rtfi0(i,j,r)))
*         = vdfm(i,j,r)*(1+rtfd0(i,j,r)) /
*                   (vdfm(i,j,r)*(1+rtfd0(i,j,r)) + vifm(i,j,r)*(1+rtfi0(i,j,r)));
*
* --- share of intermediate demand cost for product i by sector j
*     on total production cost of sector j
*     (vom = value of total output = total cost)
*
*  thetai(i,j,r) $ vom(j,r)
*         = (vdfm(i,j,r)*(1+rtfd0(i,j,r)) + vifm(i,j,r)*(1+rtfi0(i,j,r))) / vom(j,r);
*
*
* --- share of total factor cost on total production cost of sector j
*     vfm=value of factor user by sector, rft0= related tax rate, benchmark
*
*  theta_f(j,r) $ vom(j,r) = sum(ff,vfm(ff,j,r)*(1+rtf0(ff,j,r))) / vom(j,r);
*

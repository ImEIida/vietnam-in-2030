
********************************************************************************
$ontext

   GTAP in GAMS project

   GAMS file : all 2030

   @purpose  : Expected changes in labor, capital, total factor productivity by 2030 - except land endowment
   @author   :
   @date     : 05.12.15
   @since    :
   @refDoc   :
   @seeAlso  :
   @calledBy :

$offtext
********************************************************************************

Set r	/	VietNam      "Viet Nam"
		Thailand     "Thailand"
		India        "India"
		EastAsiaImp  "East Asia, rice importing"
		NearEastImp  "Near East, rice importing"
		AfricaImp    "Africa, rice importing"
		SEA          "South East Asia"
		RestofWorld  "Rest of World"	
/;	


parameters

	CapExp(r)			"Percentage Increase in Capital endowment"
	/	VietNam      	277.85,
		Thailand     	277.85,
		India        	404.5,
		EastAsiaImp  	102.26,
		NearEastImp  	239.64,
		AfricaImp    	239.64,
		SEA          	277.85,
		RestofWorld  	82.5 /;


*   --- shift capital
*
     evom("capital",r) = evom("capital",r) * (1 + CapExp(r)/100);
*

*   --- investment demand according to capital expansion
*       assumption: unchanged deprecitation share
*
     vdim(i,r) = vdim(i,r) * (1 + CapExp(r)/100);
*


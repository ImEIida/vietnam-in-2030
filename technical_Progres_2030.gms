********************************************************************************
$ontext

   GTAP in GAMS project

   GAMS file : technical progress - adjustments for the 2030 VietNam scenario

   @purpose  : Increase primary factor productivity in all/some sectors by XX percent
   @author   : W. Britz, using code by Tom Rutherford
   @date     : 24.10.13
   @since    :
   @refDoc   :
   @seeAlso  :
   @calledBy :

$offtext
********************************************************************************
*
*TECHNOLOGY
* --- technical progress
 p_techProgFac(j,"VietNam") = 1.;
 p_techProgFac(j,"SEA") = 1.;
 p_techProgFac(j,"RestofWorld") = 1.;





* --- We could also assume factor-specific technical progress. Therefore we would
* have to change the model code of mrtmcp slightly. Shouldn't be that difficult (I 
* guess)
*
* --- Explanations: 
* The technological progress factor enters the model in the Leontief production 
* function nests. It relates only to the productivity of primary factors. Often, 
* tech progress factors are estimated based on exogenous assumptions on GDP growth. 
*
* --- Leontief cost function: aggregates intermediate and value added
*                             based on input coefficient thetai and factor cost shares theta_f
*                             plus related prices
*
* cy(j,r) as the value of output of sector j in region r. Technical progress will 
* deflate the per unit factor costs and therefore it will lower the per unit production
* costs. 
*
*
*e_cy(j,r) $ (sum(i, thetai(i,j,r)) + theta_f(j,r)) ..
*
*  cy(j,r) =E= sum(i $ thetai(i,j,r), thetai(i,j,r) * ci(i,j,r)) + (theta_f(j,r)*cf(j,r)/p_techProgFac(j,r));
*
*  cy.fx(j,r) $ ( (sum(i,thetai(i,j,r)) + theta_f(j,r)) eq 0) = %homogTest%;
*
*  cy.l(j,r) $ (sum(i, thetai(i,j,r)) + theta_f(j,r))
*      = sum(i $ thetai(i,j,r), thetai(i,j,r) * ci.l(i,j,r)) + (theta_f(j,r)*cf.l(j,r));
*
*  cy.lo(j,r) $ (sum(i, thetai(i,j,r)) + theta_f(j,r)) = 1.E-6;
*
*
* --- primary factor demand
*
*e_dfm(f,j,r)  $ vfm(f,j,r) ..
*
*   dfm(f,j,r)  =E= (vfm(f,j,r)*y(j,r)/p_techProgFac(j,r)*(cf(j,r)/p_pf(f,j,r))**esubva(j));
*
*   dfm.l(f,j,r) $ vfm(f,j,r)
*                = (vfm(f,j,r)*y.l(j,r)*(cf.l(j,r)/p_pf.l(f,j,r))**esubva(j));
*
*   dfm.lo(f,j,r) $ vfm(f,j,r) = vfm(f,j,r) * 1.E-6;
*
*
* --- Zero profit condition ensures: 
* 
*prf_y(j,r) $ (vom(j,r) and py.range(j,r)) ..     cy(j,r) =e= py(j,r) * (1-rto(j,r)) / p_priceShock(j);
*
*
*
*
*
*The techn. progress factor that enters our model works by decreasing the per *unit factor costs. 
*
*(Hopefully) Illustrative Example: 
*
*For the production of one Unit of a manufactured good 50 percent of per unit *cost stem from primary factor expenditure (the other 50 percent are spent for *intermediates). 
*
*cy(mnfct, r) = 0.5*intermediate_costs + 0.5*primary_factor_costs
*
*Now, we introduce the technological progress which deflates the *primary_factor_costs: " + 0.5*primary_factor_costs/techn_progress_parameter" -> *therefore, we need the information by how many percent the factor costs for the *production of agricultural goods, paddy rice, processed rice, services and 
*manufactured goods decrease. 
*

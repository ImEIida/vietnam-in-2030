********************************************************************************
$ontext

   GTAP in GAMS project

   GAMS file : Capital endowment growth

   @purpose  : Changes in the capital stock
   @author   : W. Britz, using code by Tom Rutherford
   @date     : 05.12.15
   @since    :
   @refDoc   :
   @seeAlso  :
   @calledBy :

$offtext
********************************************************************************
*
*CAPITAL
* --- Capital stock change
   evom("Capital","VietNam") = evom("Capital","VietNam")*1.01;
   evom("Capital","SEA") = evom("Capital","SEA")*1.01;
   evom("Capital","RestofWorld") = evom("Capital","RestofWorld")*1.59;
*




* --- explanation: 
* By 2030 we expect an increase in the capital stock by XX percent. (In static CGE models
* usually such capital stock growth doesn't exist, but this has rather technical than 
* factual reasons. For 2030 we assume a growth in the capital stock by XX percent. 
*
*
* --- annotations: 
* When we change evom(f,r) the value shares will change too. We must check if these 
* new value shares are plausible in the end. See value share definitions below.
*
*
*
* --- share for each factor f on factor cost (vfm) plus factor taxes (rtf)
*
*  thetaf(f,j,r) $ sum(ff,vfm(ff,j,r)*(1+rtf0(ff,j,r)))
*         = vfm(f,j,r) * (1+rtf0(f,j,r)) / sum(ff, vfm(ff,j,r) * (1+rtf0(ff,j,r)) );
*
*
* --- share of domestic intermediate demand cost for product i by sector j
*     on total intermediate demand cost for product i by sector j
*     vdfm=value of domestic firm demand, rftf0=related ad-valorem tax rate, benchmark
*     vifm=value of imported firm demand, rfti0=related ad-valorem tax rate, benchmark
*
*  thetad(i,j,r) $ (vdfm(i,j,r)*(1+rtfd0(i,j,r)) + vifm(i,j,r)*(1+rtfi0(i,j,r)))
*         = vdfm(i,j,r)*(1+rtfd0(i,j,r)) /
*                   (vdfm(i,j,r)*(1+rtfd0(i,j,r)) + vifm(i,j,r)*(1+rtfi0(i,j,r)));
*
* --- share of intermediate demand cost for product i by sector j
*     on total production cost of sector j
*     (vom = value of total output = total cost)
*
*  thetai(i,j,r) $ vom(j,r)
*         = (vdfm(i,j,r)*(1+rtfd0(i,j,r)) + vifm(i,j,r)*(1+rtfi0(i,j,r))) / vom(j,r);
*
*
* --- share of total factor cost on total production cost of sector j
*     vfm=value of factor user by sector, rft0= related tax rate, benchmark
*
*  theta_f(j,r) $ vom(j,r) = sum(ff,vfm(ff,j,r)*(1+rtf0(ff,j,r))) / vom(j,r);
*

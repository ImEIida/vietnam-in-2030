********************************************************************************
$ontext

   GTAP in GAMS project

   GAMS file : tfp shock - as a proxy for less land
   @purpose  : Intuition: lower tfp for land because more floods
   @author   : W. Britz, using code by Tom Rutherford
   @date     : 24.10.13
   @since    :
   @refDoc   :
   @seeAlso  :
   @calledBy :

$offtext
********************************************************************************
*
* because no shocks observed when only changing land this file is added
*
*TECHNOLOGY
* --- technical progress
 p_techProgFac("pdr","VietNam") = .95;
 

*** Still plausible arguments for this decrease in tfp needed!!



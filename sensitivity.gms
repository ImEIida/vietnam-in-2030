********************************************************************************
$ontext

   GTAPinGAMS project

   GAMS file : Sensitivity GAMS

   @purpose  : Define scenarios via GAMS, use R to generate a LHS
               execute them with exp_starter,
               collect results, filter out relevant parts for meta-modelling
               and store them in a GDX container

   @author   : Wolfgang Britz
   @date     :
   @since    :
   @refDoc   :
   @seeAlso  : mrtmcp.gms
   @calledBy :

$offtext
********************************************************************************
$OffLISTING
$Onglobal
$OffLog
*
$setglobal pgmName Sensitivity analysis
*
*------------------------------------------------------------------------------
*
*   Define globals / load farm from interface or given example
*
*------------------------------------------------------------------------------
*
*   ---- define the example farm to load
*
$include 'sens_Inc.gms'
$include 'util\title_def.gms'
*
$include 'gtap6data.gms'

$if not set scrdir  $setglobal scrdir %curdir%\flags
$if not set scrdirR $setglobal scrdir %curdir%/flags

 scalar p_dummy / 0/;
*
*------------------------------------------------------------------------------
*
* Count the factors
*
*------------------------------------------------------------------------------
*
  set allFactors / esubd,esubm,esubva,etrae,shock1,shock2

                 /;

  alias(allFactors,allFactors1);

*
  set factors(allFactors) /

*
*  --- factor for experiments with dairy branch
*
$ifi not %esubdMin%            == %esubdMax%                   esubd
$ifi not %esubmMin%            == %esubmMax%                   esubm
$ifi not %esubvaMin%           == %esubvaMax%                  esubva
$ifi not %etraeMin%            == %etraeMax%                   etrae
$ifi not %shock1Min%           == %shock1Max%                  shock1
$ifi not %shock2Min%           == %shock2Max%                  shock2


  /;

$onempty

  set dummyShocks / shock1,shock2 /;


  set shocks(factors) /

*
*  --- factor for experiments with dairy branch
*
$ifi not %shock1Min%           == %shock1Max%                  shock1
$ifi not %shock2Min%           == %shock2Max%                  shock2


  /;

  set s1 / s1*s%nShock1%/;
  set s2 / s1*s%nShock2%/;

 alias(factors,factors1);

 set loUpStddev / lo,up,StdDev /;
 parameter p_loUpStdDev(*,loUpStdDev);

$ifi not %esubdMin%            == %esubdMax%         p_loUpStdDev("esubd","lo")  = %esubdMin%;    p_loUpStdDev("esubd","up")  = %esubdMax%;   p_loUpStdDev("esubd","stdDev")  = %esubdStdDev%;
$ifi not %esubmMin%            == %esubmMax%         p_loUpStdDev("esubm","lo")  = %esubmMin%;    p_loUpStdDev("esubm","up")  = %esubmMax%;   p_loUpStdDev("esubm","stdDev")  = %esubmStdDev%;
$ifi not %esubvaMin%           == %esubvaMax%        p_loUpStdDev("esubva","lo") = %esubvaMin%;   p_loUpStdDev("esubva","up") = %esubvaMax%;  p_loUpStdDev("esubva","stdDev") = %esubvaStdDev%;
$ifi not %etraeMin%            == %etraeMax%         p_loUpStdDev("etrae","lo")  = %etraeMin%;    p_loUpStdDev("etrae","up")  = %etraeMax%;   p_loUpStdDev("etrae","stdDev")  = %etraeStdDev%;
$ifi not %shock1Min%           == %shock1Max%        p_loUpStdDev("shock1","lo") = %shock1Min%;   p_loUpStdDev("shock1","up")  = %shock1Max%; p_loUpStdDev("shock1","stdDev") = %shock1StdDev%;
$ifi not %shock2Min%           == %shock2Max%        p_loUpStdDev("shock2","lo") = %shock2Min%;   p_loUpStdDev("shock2","up")  = %shock2Max%; p_loUpStdDev("shock2","stdDev") = %shock2StdDev%;

*
*------------------------------------------------------------------------------
*
* Use R to define the DOE
*
*------------------------------------------------------------------------------
*
$batinclude 'util\title.gms' "'Generate experiments with R script ... '"

$setglobal r "%curdir%\..\r-2.15.1\bin\rscript.exe"

 file rIncFile / "%curDir%\rBridge\incFile.r" /;
 put rIncFile;
$setglobal outputFile   "%scrdirR%/fromR"
$setglobal inputFile    "%scrdirR%/toR.gdx"


 parameter p_cor(*,*);
 option kill=p_cor;


 put ' plotFile    <- "%scrdirR%/lhs.pdf"; '/;
 put ' outputFile  <- "%outputFile%"; '/;
 put ' inputFile   <- "%inputFile%"; '/;
 put ' useLoUp     <- "true"; '/;
 put ' weibullCorr <- %weibullCorr%;' /;
 put ' distType    <- "%distType%";' /;

      parameter p_loUpStdDevForR(*,loUpStddev);
$iftheni %uniformAcrossSectors%==ON
 put ' secondDim   <- "false"; '/;
 put ' loadFactorNames   <- "false"; '/;
      alias(factors,factorsForR,factorsForR1);
      p_loUpStdDevForR(factors,loUpStddev) = p_loUpStdDev(factors,loUpStddev);

      set fr_f_i(*,*,*);option kill=fr_f_i;

      p_cor(factorsForR,factorsForR1) = %correltParam% + 1.E-8;


$else

$eval last max(card(i),card(f),card(s1),card(s2))
$eval n card(factors) * %last%

      set factorsForR /fac1*fac%n%/;
      alias(factorsForR,factorsForR1);

      set usedRFactors(*);


      set lastSet / l1*l%last% /;
      set fr_f_i(*,*,*);
      set fr_f(*,factors);
      set fr_i(*,*);
      scalar ipos; ipos=1;
      loop( (factors,lastSet),
         if ( sameas(factors,"shock1"),
            fr_f_i(factorsForR,factors,s1) $ ( (factorsForR.pos eq ipos) $ (s1.pos eq lastSet.pos)) = yes;
         elseif ( sameas(factors,"shock2") ),
            fr_f_i(factorsForR,factors,s2) $ ( (factorsForR.pos eq ipos) $ (s2.pos eq lastSet.pos)) = yes;
         elseif ( not sameas(factors,"etrae")),
            fr_f_i(factorsForR,factors,i) $ ( (factorsForR.pos eq ipos) $ (i.pos eq lastSet.pos)) = yes;
         else
            fr_f_i(factorsForR,factors,f) $ ( (factorsForR.pos eq ipos) $ (f.pos eq lastSet.pos)) = yes;
         );

         fr_i(factorsForR,i) $ ( (factorsForR.pos eq ipos) $ (i.pos eq lastSet.pos)) = YES;
         fr_f(factorsForR,factors) $ (factorsForR.pos eq ipos) = yes;
         p_loUpStdDevForR(factorsForR,loUpstdDev)  $ (factorsForR.pos eq ipos) = p_loUpStdDev(factors,loUpStddev);

         ipos = ipos+1;
      );

      usedRFactors(factorsForR) $ sum(fr_f_i(factorsForR,factors,s1),1) = YES;
      usedRFactors(factorsForR) $ sum(fr_f_i(factorsForR,factors,s2),1) = YES;
      usedRFactors(factorsForR) $ sum(fr_f_i(factorsForR,factors,i),1) = YES;
      usedRFactors(factorsForR) $ sum(fr_f_i(factorsForR,factors,f),1) = YES;
      option kill=factorsForR;
      factorsForR(usedRFactors) = YES;

      p_cor(factorsForR,factorsForR1) = 1.E-8;

      loop( factors,
          p_cor(factorsForR,factorsForR1) $ (fr_f(factorsForR,factors) $ fr_f(factorsForR1,factors)) = %correltSecFac%+1.E-8;
      );

      p_cor(factorsForR,factorsForR1) = min(1, p_cor(factorsForR,factorsForR1) + %correltParam%+1.E-8);




*     abort powerSetX;

 put ' secondDim         <- "false"; '/;
 put ' loadFactorNames   <- "true"; '/;
$endif
 put ' useColors   <- "true"; '/;
 put ' maxRunTime  <- %maxRunTime%; '/;

if ( card(factorsForR) > 1,
   put ' useCorr     <- "true"; '/;
else
   put ' useCorr     <- "false"; '/;


);

 putclose;


$eval nDraws round(%nDraws%)
 scalar p_n;p_n=%nDraws%;

$ifthene %nDraws%<100

$setglobal test "0%nDraws%"

$else

$setglobal test "%nDraws%"

$endif
 set meand / meand,mean /;
 set draws              / mean,draws001*draws%test%/;
 set drawsWoM(draws)   / draws001*draws%test%/;


*
* --- replace zeros by 1.E-8
*
  p_cor(factorsForR,factorsForR) = 1;
  display p_cor;

 set factor_name(*,*) / name.factorsForR /;
 set dim2_name(*,*)   / name.i /;
 set scen_name(*,*) / name."%scendes%"/;

$setglobal outputFile   "%scrdir%/fromR"
$setglobal inputFile    "%scrdir%/toR.gdx"

 execute_unload "%inputFile%" p_n,factor_name,dim2_name,scen_name,factorsForR,p_cor,p_loUpStdDevForR,i,fr_f_i;

$setglobal rFile "%curDir%\rbridge\lhs.r"
$if not exist %rexe% abort "R script.exe not found at " %rexe%;

$if exist "%scrdir%\lhs.pdf" execute "del %scrdir%\lhs.pdf";


$if exist %rexe% execute "%rexe% %rFile% %curDir%\rBridge\incFile.r";


$iftheni %uniformAcrossSectors%==ON
  parameter p_doe(*,*);
  execute_load "%outputFile%_doe" p_doe;
  p_doe("mean",factors) = 1;
$else
 parameter p_draws(*,*);
 execute_load "%outputFile%_doe" p_draws=p_doe;

  parameter p_doe(*,*,*);
  p_doe(draws,factors,i)        = sum(fr_f_i(factorsForR,factors,i),   p_draws(draws,factorsForR));
  p_doe(draws,factors,f)        = sum(fr_f_i(factorsForR,factors,f),   p_draws(draws,factorsForR));
  p_doe(draws,"shock1",s1)      = sum(fr_f_i(factorsForR,"shock1",s1), p_draws(draws,factorsForR));
  p_doe(draws,"shock2",s2)      = sum(fr_f_i(factorsForR,"shock2",s2), p_draws(draws,factorsForR));
  p_doe("mean",factors,i) = 1;
  p_doe("mean",factors,f) = 1;
  p_doe("mean",factors,s1) = 1;
  p_doe("mean",factors,s2) = 1;
$endif

  display p_doe;

*------------------------------------------------------------------------------
*
* Entropy estimator (estimates GAUSSIAN weights)
*
*------------------------------------------------------------------------------
*

 positive variables v_p(draws);
 v_p.up(draws) = 1;
 v_p.lo(draws) = 1/card(draws) * 0.1;

 variable v_e;

 v_p.l(draws) = 1/(card(draws)-1);
 v_p.fx("mean") = 0;

 display p_doe;


$iftheni %useEntropyEstimator%==ON


 equations e_ent,e_unitP,e_unitFI,e_unitFX,e_unitiI,e_unitiX;
*
* --- entroy definition
*
  e_ent   ..  v_e =E= -sum(draws $ v_p.range(draws), v_p(draws) * log(v_p(draws)));
*
* --- probs add up to unity
*
  e_unitP ..  sum(drawsWoM, v_p(drawsWoM)) =E= 1;

  scalar p_relBound / 0.0 /;
*
* --- each factors must have a mean of unity
*
$iftheni.unif %uniformAcrossSectors%==ON
  e_unitFI(factors)   ..  sum(drawsWoM, v_p(drawsWoM) * p_doe(drawsWoM,factors)) =l= 1. + p_relBound;
  e_unitFX(factors)   ..  sum(drawsWoM, v_p(drawsWoM) * p_doe(drawsWoM,factors)) =g= 1. - p_relBound;
  model m_ent / e_ent, e_unitP,e_unitFI,e_unitFX /;
$else.unif
  e_unitII(factors,i) $ sum(drawsWoM,p_doe(drawsWoM,factors,i))  ..  sum(drawsWoM, v_p(drawsWoM) * p_doe(drawsWoM,factors,i)) =l= 1. + p_relBound;
  e_unitIX(factors,i) $ sum(drawsWoM,p_doe(drawsWoM,factors,i))  ..  sum(drawsWoM, v_p(drawsWoM) * p_doe(drawsWoM,factors,i)) =g= 1. - p_relBound;

  e_unitFI(factors,f) $ sum(drawsWoM,p_doe(drawsWoM,factors,f)) ..  sum(drawsWoM, v_p(drawsWoM) * p_doe(drawsWoM,factors,f)) =l= 1. + p_relBound;
  e_unitFX(factors,f) $ sum(drawsWoM,p_doe(drawsWoM,factors,f)) ..  sum(drawsWoM, v_p(drawsWoM) * p_doe(drawsWoM,factors,f)) =g= 1. - p_relBound;

  model m_ent / e_ent, e_unitP,e_unitII,e_unitIX,e_unitFI,e_unitFX /;
  m_ent.solprint=1;
$endif.unif


  solve m_ent using NLP maximizing v_e;
  if ( m_ent.sumInfes > 0,
     p_relBound = 0.01;
     solve m_ent using NLP maximizing v_e;
  );
  if ( m_ent.sumInfes > 0,
     p_relBound = 0.025;
     solve m_ent using NLP maximizing v_e;
  );
  if ( m_ent.sumInfes > 0,
     p_relBound = 0.05;
     solve m_ent using NLP maximizing v_e;
  );

  if ( m_ent.sumInfes > 0,
    abort "could not ensure mean draws between 0.95 at 1.05 relative to given parameters - adjust ranges or increase number of experiments");
$endif

  display v_p.l;


$ifi "%onlyGenerateDraws%" == "ON" $exit

*------------------------------------------------------------------------------
*
* Define scenarios to run
*
*------------------------------------------------------------------------------
*




 parameter p_scenParam(draws,factors) "Numerical values for the scenario specific items";

*
*------------------------------------------------------------------------------
*
* Execution and data collection loop
*
*------------------------------------------------------------------------------
*
$eval parallelThreads round(%parallelThreads%)

$iftheni %parallelThreads% == 1

$setglobal runParallel NO

$else

$setglobal runParallel YES

$endif
*
 scalar iLoop;

 batch.lw=0;

*


 execute "mkdir %curDir%\flags";
 p_dummy = sleep(1.0);
*
* ------------------------------------------------------------------------------
*
*     Execution loop
*
* ------------------------------------------------------------------------------
*

 batch.lw=0;
 scalar iLoop;

 file scenFile / %scrdir%\curScen.gms /;
 scenfile.nd=10;


$batinclude 'util\title.gms' "'Run GTAP for benchmark ... '"
*
 execute "del %curDir%\flags\*.flag";
 execute "del %scrdir%\mrtmcp_savepoint.*";

 put_utility batch 'msglog' / '%GAMSPATH%\gams.exe %CURDIR%\model\mrtmcp.gms s=%scrdir%\mrtmcp_savepoint --mode=savepoint'
 execute "=%GAMSPATH%\gams.exe %CURDIR%\model\mrtmcp.gms s=%scrdir%\mrtmcp_savepoint --mode=savepoint --lo=2";



 iLoop=0;

 scalar p_value;

*
   loop(draws,
      iLoop = iLoop + 1;
*
*        --- append run specific parameters to settings files
*
*
*  --- copy content of current scen file into new one
*      via OS command
*
      execute "type %curDir%\model\sens_inc.gms > %scrDir%\curScen.gms";
*
*  --- put statements will append to the new scen file
*      and overwrite standard setting
*
      put scenFile;
      scenFile.ap = 1;
      scenFile.lw = 0;


$ifi not %shock1Min%           == %shock1Max%   put "parameter shock1(*);set s1/s1*s%nshock1%/;" /;
$ifi not %shock2Min%           == %shock2Max%   put "parameter shock2(*);set s2/s1*s%nshock2%/;" /;

*
*  --- append scen specific parameters to include file
*
      loop(factors,

$iftheni %uniformAcrossSectors%==ON


          if ( sameas(factors,"shock1"),
             put  " "factors.tl,"(s1) =",p_doe(draws,factors)";" /;
          elseif ( sameas(factors,"shock2")),
             put  " "factors.tl,"(s2) =",p_doe(draws,factors)";" /;
          elseif ( not sameas(factors,"etrae")),
             put  " "factors.tl,"(i) =",p_doe(draws,factors),"*",factors.tl,"(i) ;" /;
          else
             put  " "factors.tl,"(f) =",p_doe(draws,factors),"*",factors.tl,"(f) ;" /;
          );
$else

          loop(i $ ( not (sameas(factors,"etrae") or sameas(factors,"shock1") or sameas(factors,"shock2")) ),
             put  " "factors.tl,"('",i.tl,"') =",p_doe(draws,factors,i),"*",factors.tl,"('",i.tl,"');" /;
          );

          loop(f $ sameas(factors,"etrae"),
            put  " "factors.tl,"('",f.tl,"') =",p_doe(draws,factors,f),"*",factors.tl,"('",f.tl,"');" /;
          );

          loop(s1 $ sameas(factors,"shock1"),
            put  " "factors.tl,"('",s1.tl,"') =",p_doe(draws,factors,s1)";" /;
          );
          loop(s2 $ sameas(factors,"shock2"),
            put  " "factors.tl,"('",s2.tl,"') =",p_doe(draws,factors,s2)";" /;
          );



$endif
      );

      put "$SETGLOBAL scenDes curScen " /;
      putClose scenFile;

      put_utility batch 'shell' / "type %scrdir%\curScen.gms > %curDir%\model\settings_"draws.tl".gms";
*
*        The batch script will continue to block further execution until less than
*        %parallelThreads% flag files are present in the directory. That will ensure that not more than
*        %parallelThreads% GAMS processes run in parallel on the machine
*
$batinclude 'util\title.gms' "'Waiting for block of scenario to finish: '" iLoop:0:0 "' of '" card(draws):0:0
      execute "=%curdir%\model\util\taskSyncNFiles.bat 1 3600 %curDir%\flags\*.flag %parallelThreads%";
*
*    --- generate a flag file which will be deleted by the GAMS process upon completion,
*        it steers the further execution of the loop
*
      put_utility batch 'shell'   / "echo test > %curDir%\flags\"draws.tl".flag";
*
*     --- delete the listing to that we do not read later old stuff
*
      put_utility batch 'shell'   / 'if exist ..\results\scens\res_',draws.tl,'.gdx ',
                        ' del ..\results\scens\res_',draws.tl,'.gdx ';
*
*     --- execute exp_starter as a seperate program, no wait, program will delete a flag at the end to signal that it is ready
*
      put_utility batch 'msglog' / 'start /B /NORMAL %GAMSPATH%\gams.exe %CURDIR%\model\mrtmcp.gms r=%scrdir%\mrtmcp_savepoint --exp="'draws.tl'"'
                          ' -maxProcDir=255 -output='draws.tl'.lst %gamsarg% --pgmName="'draws.tl' (',iLoop:0:0,' of ',card(draws):0:0,')"';

      put_utility batch 'shell'   / 'start /B /NORMAL %GAMSPATH%\gams.exe %CURDIR%\model\mrtmcp.gms r=%scrdir%\mrtmcp_savepoint --exp="'draws.tl'"'
                           ' --iScen='iLoop:0:0' -maxProcDir=255 -output='draws.tl'.lst execerr=100',
                           ' lo=3 --pgmName="'draws.tl' (',iLoop:0:0,' of ',card(draws):0:0,')"  %gamsarg%'
$batinclude 'util\title.gms' "'Scenario '" iLoop:0:0 "' started of '" card(draws):0:0
   );

*
* --- wait until remaining flag files are deleted
*
$batinclude 'util\title.gms' "'Waiting for remaining scenarios to be done .. '"
*
* --- wait maximal 60 Minutes times 60 seconds = max 3.600 seconds for the remaining
*     GAMS processes to finish
*
*
 execute "=%curdir%\model\util\taskSync.bat 1 3600 %curDir%\flags\*.flag";
 execute "rm  %curDir%\flags";

*
* ------------------------------------------------------------------------------
*
*     collection loop
*
* ------------------------------------------------------------------------------
*

   set  PQV /P "Price", Q "Quantity", V "Value", T "tax rate", G "Tax income","Meta" /;
   parameter p_results(*,pqv,*,*,*,*);

   set samRows / set.i,set.f /;
   set samCols / set.i,hou,gov,intTrs,set.s,inv /;
   set oris    / dom,imp,tot /;

    set incPos / totf,vb,o,fd,fi,f,pd,pi,gd,gi,xs,ms /;

    set taxPos / o,fd,fi,f,pd,pi,gd,gi,xs,ms /;

   set dummy / h,intTrs,pop,tax,inc,g,c,cg,ra,outTax,facTax,intTax,int,totf,exp,del,imp,dom,use,totp,tots,Wor,SumInfes,NumInfes,out /;

   alias (u,u1,u2,u3,*);
   option kill=p_results;


 iLoop=0;
 Loop( draws,

$batinclude 'util\title.gms' "'Collecting result '" iLoop:0:0 "' of '" card(draws):0:0
*
*     --- delete listing file if result file is available
*
      put_utility batch 'shell'   / 'if exist %resdir%\scens\res_',draws.tl,'.gdx del %curdir%\model\settings_'draws.tl'.gms';
*
*     --- set name of result file (comprises last year)
*
      put_utilities batch 'gdxin' / '%resdir%\scens\res_'draws.tl'.gdx'
*
      if ( execerror eq 0,

          iLoop = iLoop + 1;
*
*         --- load the result
*
          execute_loadpoint p_results;

          p_results(u,pqv,u1,u2,u3,"meand") $ p_results(u,pqv,u1,u2,u3,draws)
           = p_results(u,pqv,u1,u2,u3,"meand") + p_results(u,pqv,u1,u2,u3,draws) * v_p.l(draws);

          if ( p_results("Wor","Meta","NumInfes","tots","dom",draws) eq 0,
             put_utility batch 'shell'   / 'if exist %resdir%\scens\res_',draws.tl,'.gdx del %curdir%\model\'draws.tl'.lst';
          );
          put_utility batch 'shell'   / 'if exist %resdir%\scens\res_',draws.tl,'.gdx del %resdir%\scens\res_',draws.tl,'.gdx';
      else

         put_utilities batch 'msglog' / '%resdir%\scens\res_',draws.tl'.gdx does not exist - check listing';
         execerror = 0;
      );
 );
*
* --- Store to disk (so that also intermediate results can be inspected)
*
$if not setglobal scenDes $setglobal scenDes %shock_underScores%
$if "%scenDes%"=="" $setglobal scenDes %shock_underScores%

   display p_results;

  execute_unload '..\results\sens\res_%scenDes%.gdx' s_meta,p_results;

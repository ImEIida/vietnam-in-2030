
********************************************************************************
$ontext

   GTAP in GAMS project

   GAMS file : all 2030

   @purpose  : Expected changes in labor, capital, total factor productivity by 2030 - except land endowment
   @author   :
   @date     : 05.12.15
   @since    :
   @refDoc   :
   @seeAlso  :
   @calledBy :

$offtext
********************************************************************************

Set r	/	VietNam      "Viet Nam"
		Thailand     "Thailand"
		India        "India"
		EastAsiaImp  "East Asia, rice importing"
		NearEastImp  "Near East, rice importing"
		AfricaImp    "Africa, rice importing"
		SEA          "South East Asia"
		RestofWorld  "Rest of World"	
/;	



*	LndChg(r)			"Percentage Change in Land endowment"

parameters

	PopExp(r)			"Population growth"
	/	VietNam      	24.86,
		Thailand     	24.86,
		India        	30.97,
		EastAsiaImp  	7.38,
		NearEastImp  	54.87,
		AfricaImp    	54.87,
		SEA          	24.86,
		RestofWorld  	25.58 /;


*   --- population
*
     pop(r) = pop(r) * (1 + PopExp(r)/100);
*
*   --- shift gov demand with population
*
     vgm(r) = vgm(r) * (1 + PopExp(r)/100);
     vdgm(i,r) = vdgm(i,r) * (1 + PopExp(r)/100);
*
*     rtf(f,j,r) $ ( rtf(f,j,r) < 0) = rtf(f,j,r)*0.5;
*
*
*	--- symmetric tech. progress (non-sector specific)
*


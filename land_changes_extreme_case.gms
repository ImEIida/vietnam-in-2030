********************************************************************************
$ontext

   GTAP in GAMS project

   GAMS file : global land endowment change

   @purpose  : Changes in the Land endowment - based on expectations
   @author   : 
   @date     : 05.12.15
   @since    :
   @refDoc   :
   @seeAlso  :
   @calledBy :

$offtext
********************************************************************************
*
*** !!! ATTENTION !!!
*** Very extreme assumptions - only for illustration purpose
*
* --- Land endowment change
   evom("land",r) = evom("land",r) * 0.50;
   


* --- explanation: 
* By 2030 we expect changes in the land endowment caused by sea level rises and other 
* effects of climate change. 
*
* --- evom corresponds to the total expenditure for primary factor f (here: land) for 
* all sectors in regions. In the model it is defined by: 
*
*        Market clearance associated with primary factors:
*
* mkt_pf(f,r) $ (evom(f,r) $ (not fpf(f))) ..    evom(f,r) =e= sum(j $ vfm(f,j,r), dfm(f,j,r)) $ mf(f) + (evom(f,r)*ft(f,r))$sf(f);
*
* 
*
*
* --- annotations: 
* When we change evom(f,r) the value shares will change too. We must check if these 
* new value shares are plausible in the end. See value share definitions below.
*
*
*
* --- share for each factor f on factor cost (vfm) plus factor taxes (rtf)
*
*  thetaf(f,j,r) $ sum(ff,vfm(ff,j,r)*(1+rtf0(ff,j,r)))
*         = vfm(f,j,r) * (1+rtf0(f,j,r)) / sum(ff, vfm(ff,j,r) * (1+rtf0(ff,j,r)) );
*
*
* --- share of domestic intermediate demand cost for product i by sector j
*     on total intermediate demand cost for product i by sector j
*     vdfm=value of domestic firm demand, rftf0=related ad-valorem tax rate, benchmark
*     vifm=value of imported firm demand, rfti0=related ad-valorem tax rate, benchmark
*
*  thetad(i,j,r) $ (vdfm(i,j,r)*(1+rtfd0(i,j,r)) + vifm(i,j,r)*(1+rtfi0(i,j,r)))
*         = vdfm(i,j,r)*(1+rtfd0(i,j,r)) /
*                   (vdfm(i,j,r)*(1+rtfd0(i,j,r)) + vifm(i,j,r)*(1+rtfi0(i,j,r)));
*
* --- share of intermediate demand cost for product i by sector j
*     on total production cost of sector j
*     (vom = value of total output = total cost)
*
*  thetai(i,j,r) $ vom(j,r)
*         = (vdfm(i,j,r)*(1+rtfd0(i,j,r)) + vifm(i,j,r)*(1+rtfi0(i,j,r))) / vom(j,r);
*
*
* --- share of total factor cost on total production cost of sector j
*     vfm=value of factor user by sector, rft0= related tax rate, benchmark
*
*  theta_f(j,r) $ vom(j,r) = sum(ff,vfm(ff,j,r)*(1+rtf0(ff,j,r))) / vom(j,r);
*
*
*


********************************************************************************
$ontext

   GTAP in GAMS project

   GAMS file : all 2030

   @purpose  :
   @author   :
   @date     : 05.12.15
   @since    :
   @refDoc   :
   @seeAlso  :
   @calledBy :

$offtext
********************************************************************************

Set r	/	VietNam      "Viet Nam"
		Thailand     "Thailand"
		India        "India"
		EastAsiaImp  "East Asia, rice importing"
		NearEastImp  "Near East, rice importing"
		AfricaImp    "Africa, rice importing"
		SEA          "South East Asia"
		RestofWorld  "Rest of World"	
/;	

Set j	/	pdr          "Paddy rice"
		wht          "Wheat"
		gro          "Other cereals"
		crops        "Other crops"
		Anim         "Animals"
		pcr          "Processed rice"
		AnimPrc      "Animal processing"
		OthFoodPrc   "Other food processing"
		CoaOilGas    "Fossil fuels"
		Mnfc         "Manufacturing"
		Services     "Other Services" 
/;


parameters

	tfp_Sec(r)			"Region specific techn progress"	
	/	VietNam      	35.75,
		Thailand     	34.59,
		India        	157.61,
		EastAsiaImp  	47.36,
		NearEastImp  	20.11,
		AfricaImp    	20.11,
		SEA          	34.59,
		RestofWorld  	51.54 /;


*
*
*	--- symmetric tech. progress (non-sector specific)
*
	p_techProgFac(j,r) = (1 + tfp_Sec(r)/100);
	p_techProgFac("PDR",r) = p_techProgFac("pdr",r)*0.9;

*

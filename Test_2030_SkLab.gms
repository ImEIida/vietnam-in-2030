
********************************************************************************
$ontext

   GTAP in GAMS project

   GAMS file : all 2030

   @purpose  : Expected changes in labor, capital, total factor productivity by 2030 - except land endowment
   @author   :
   @date     : 05.12.15
   @since    :
   @refDoc   :
   @seeAlso  :
   @calledBy :

$offtext
********************************************************************************

Set r	/	VietNam      "Viet Nam"
		Thailand     "Thailand"
		India        "India"
		EastAsiaImp  "East Asia, rice importing"
		NearEastImp  "Near East, rice importing"
		AfricaImp    "Africa, rice importing"
		SEA          "South East Asia"
		RestofWorld  "Rest of World"	
/;	


Parameters
 
	SkLabExp(r)			"Percentage Increase in skLabor endowment"
	/	VietNam		129.1,
		Thailand	129.1,
		India        	148.11,
		EastAsiaImp  	64.96,
		NearEastImp  	155.34,
		AfricaImp    	155.34,
		SEA          	129.1,
		RestofWorld  	46.7 /;


parameters

	PopExp(r)			"Population growth"
	/	VietNam      	24.86,
		Thailand     	24.86,
		India        	30.97,
		EastAsiaImp  	7.38,
		NearEastImp  	54.87,
		AfricaImp    	54.87,
		SEA          	24.86,
		RestofWorld  	25.58 /;


	

*
*   --- shift labour
*
     evom("skLab",r)   = evom("skLab",r)   * (1 + SkLabExp(r)/100);
*     evom("unSkLab",r) = evom("unSkLab",r) * (1 + UnskLabExp(r)/100);
*
*   --- shift capital
*
*     evom("capital",r) = evom("capital",r) * (1 + CapExp(r)/100);
*
*   --- shift land
*
*     evom("land",r) = evom("land",r) * (1 + LndChg(r)/100);
*
*   --- shift natural resources
*
*     evom("natres",r) = evom("natres",r) * (1 + NatResExp(r)/100);
*
*     vfm(f,j,r) $ evom(f,r) = vfm(f,j,r) * evom(f,r)/sum(i, vfm(f,i,r));
*
*
*   --- investment demand according to capital expansion
*       assumption: unchanged deprecitation share
*
*     vdim(i,r) = vdim(i,r) * (1 + CapExp(r)/100);
*
*   --- population
*
     pop(r) = pop(r) * (1 + PopExp(r)/100);
*
*   --- shift gov demand with population
*
     vgm(r) = vgm(r) * (1 + PopExp(r)/100);
     vdgm(i,r) = vdgm(i,r) * (1 + PopExp(r)/100);
*
*     rtf(f,j,r) $ ( rtf(f,j,r) < 0) = rtf(f,j,r)*0.5;
*
*
*	--- symmetric tech. progress (non-sector specific)
*
*	p_techProgFac(j,r) = (1 + tfp_Sec(r)/100);
*
*
*   --- relative t.p. in agriculture
*       (to offset effect of fixed land)
*
*     set agr(j);
*     set poss_agr /
*
*     pdr
*     wht
*     gro
*     v_f
*     osd
*     c_b
*     pfb
*     ocr
*     ctl
*     oap
*     rmk
*     wol

*     agri

*     /;


*     agr(j) $ sum(sameas(j,poss_agr),1)  = yes;


*     p_techProgFac(agr,r) = (1 + %tfpAgr%/100);





*
* ENERGY PRICE SHOCK
* --- Price increase of fossil fuels (via tech Prog factor)
*
*p_techProgFac("CoaOilGas",r) = 0.95;
*





* --- explanation: 
* By 2030 we expect population growth by XX percent in VietNam, SEA and RoW. 
* We assume the relative growth of skilled labor to be the same/higher/lower than
* growth of unskilled labor in VietNam, SEA, RoW
*
*
* --- annotations: 
* When we change evom(f,r) the value shares will change too. We must check if these 
* new value shares are plausible in the end. See value share definitions below.
*
*
*
* --- share for each factor f on factor cost (vfm) plus factor taxes (rtf)
*
*  thetaf(f,j,r) $ sum(ff,vfm(ff,j,r)*(1+rtf0(ff,j,r)))
*         = vfm(f,j,r) * (1+rtf0(f,j,r)) / sum(ff, vfm(ff,j,r) * (1+rtf0(ff,j,r)) );
*
*
* --- share of domestic intermediate demand cost for product i by sector j
*     on total intermediate demand cost for product i by sector j
*     vdfm=value of domestic firm demand, rftf0=related ad-valorem tax rate, benchmark
*     vifm=value of imported firm demand, rfti0=related ad-valorem tax rate, benchmark
*
*  thetad(i,j,r) $ (vdfm(i,j,r)*(1+rtfd0(i,j,r)) + vifm(i,j,r)*(1+rtfi0(i,j,r)))
*         = vdfm(i,j,r)*(1+rtfd0(i,j,r)) /
*                   (vdfm(i,j,r)*(1+rtfd0(i,j,r)) + vifm(i,j,r)*(1+rtfi0(i,j,r)));
*
* --- share of intermediate demand cost for product i by sector j
*     on total production cost of sector j
*     (vom = value of total output = total cost)
*
*  thetai(i,j,r) $ vom(j,r)
*         = (vdfm(i,j,r)*(1+rtfd0(i,j,r)) + vifm(i,j,r)*(1+rtfi0(i,j,r))) / vom(j,r);
*
*
* --- share of total factor cost on total production cost of sector j
*     vfm=value of factor user by sector, rft0= related tax rate, benchmark
*
*  theta_f(j,r) $ vom(j,r) = sum(ff,vfm(ff,j,r)*(1+rtf0(ff,j,r))) / vom(j,r);
*
*
*
*ENERGY PRICE SHOCK: Calculation of the percentage increase
* Price_multiplier: 1+%Increase=1/p_techProgFac <=> p_techProgFac=1/(1+%Increase)
*
*

********************************************************************************
$ontext

   GTAP in GAMS project

   GAMS file : sector_specific_factor_price

   @purpose  : Manipulation of sector specific factor prices (i.e. land for pdr production)
   @author   : 
   @date     : 05.12.15
   @since    :
   @refDoc   :
   @seeAlso  :
   @calledBy :

$offtext
********************************************************************************
*
*** Data source: guesses
*
*
*SECTOR SPECIFIC FACTOR PRICES
*Variables
* p_pf(f,j,r)    "Sector specific factor prices"

p_pf("land","pdr","VietNam")=p_pf("land","pdr","VietNam")*1.5;






* --- explanation: 
********************************************************************************
$ontext

   GTAP in GAMS project

   GAMS file : all 2030

   @purpose  : Expected changes in labor, capital, total factor productivity by 2030 - except land endowment
   @author   :
   @date     : 05.12.15
   @since    :
   @refDoc   :
   @seeAlso  :
   @calledBy :

$offtext
********************************************************************************
*
*** HERE:Data taken from data file in the Dropbox
*
*LABOR
* --- skilled labor
   evom("SkLab","VietNam") = evom("Sklab","VietNam")*1.6;
   evom("SkLab","SEA") = evom("sklab","SEA")*1.518;
   evom("SkLab","EastAsiaImp") = evom("sklab","EastAsiaImp")*1.518;
   evom("SkLab","Thailand") = evom("sklab","Thailand")*1.518;
   evom("SkLab","India") = evom("sklab","India")*1.518;
   evom("SkLab","RestofWorld") = evom("sklab","RestofWorld")*.972;
   evom("SkLab","AfricaImp") = evom("sklab","AfricaImp")*.972;
   evom("SkLab","NearEastImp") = evom("sklab","NearEastImp")*.972;
* --- unskilled labor
evom("SkLab","VietNam") = evom("UnSklab","VietNam")*.86;
   evom("UnSkLab","SEA") = evom("Unsklab","SEA")*1.17;
   evom("UnSkLab","EastAsiaImp") = evom("Unsklab","EastAsiaImp")*1.17;
   evom("UnSkLab","Thailand") = evom("Unsklab","Thailand")*1.17;
   evom("UnSkLab","India") = evom("Unsklab","India")*1.17;
   evom("UnSkLab","RestofWorld") = evom("Unsklab","RestofWorld")*1.059;
   evom("UnSkLab","AfricaImp") = evom("Unsklab","AfricaImp")*1.059;
   evom("UnSkLab","NearEastImp") = evom("Unsklab","NearEastImp")*1.059;

*
*CAPITAL
* --- Capital stock change
   evom("Capital","EastAsiaImp") = evom("Capital","EastAsiaImp")*1.983;
   evom("Capital","Thailand") = evom("Capital","Thailand")*1.983;
   evom("Capital","India") = evom("Capital","India")*1.983;
   evom("Capital","RestofWorld") = evom("Capital","RestofWorld")*1.551;
   evom("Capital","AfricaImp") = evom("Capital","AfricaImp")*1.551;
   evom("Capital","NearEastImp") = evom("Capital","NearEastImp")*1.551;
   evom("Capital","VietNam") = evom("Capital","VietNam")*1.983;
   evom("Capital","SEA") = evom("Capital","SEA")*1.983;
   evom("Capital","RestofWorld") = evom("Capital","RestofWorld")*1.551;
*
*
*TECHNOLOGY
* --- technical progress
 p_techProgFac(j,"VietNam") = 1.258;
 p_techProgFac(j,"SEA") = 1.258;
 p_techProgFac(j,"RestofWorld") = 1.265;
 p_techProgFac(j,"EastAsiaImp") = 1.258;
 p_techProgFac(j,"Thailand") = 1.258;
 p_techProgFac(j,"India") = 1.258;
 p_techProgFac(j,"AfricaImp") = 1.265;
 p_techProgFac(j,"NearEastImp") = 1.265;


*
* ENERGY PRICE SHOCK
* --- Price increase of fossil fuels (via tech Prog factor)
*
*p_techProgFac("CoaOilGas",r) = 0.95;
*





* --- explanation: 
* By 2030 we expect population growth by XX percent in VietNam, SEA and RoW. 
* We assume the relative growth of skilled labor to be the same/higher/lower than
* growth of unskilled labor in VietNam, SEA, RoW
*
*
* --- annotations: 
* When we change evom(f,r) the value shares will change too. We must check if these 
* new value shares are plausible in the end. See value share definitions below.
*
*
*
* --- share for each factor f on factor cost (vfm) plus factor taxes (rtf)
*
*  thetaf(f,j,r) $ sum(ff,vfm(ff,j,r)*(1+rtf0(ff,j,r)))
*         = vfm(f,j,r) * (1+rtf0(f,j,r)) / sum(ff, vfm(ff,j,r) * (1+rtf0(ff,j,r)) );
*
*
* --- share of domestic intermediate demand cost for product i by sector j
*     on total intermediate demand cost for product i by sector j
*     vdfm=value of domestic firm demand, rftf0=related ad-valorem tax rate, benchmark
*     vifm=value of imported firm demand, rfti0=related ad-valorem tax rate, benchmark
*
*  thetad(i,j,r) $ (vdfm(i,j,r)*(1+rtfd0(i,j,r)) + vifm(i,j,r)*(1+rtfi0(i,j,r)))
*         = vdfm(i,j,r)*(1+rtfd0(i,j,r)) /
*                   (vdfm(i,j,r)*(1+rtfd0(i,j,r)) + vifm(i,j,r)*(1+rtfi0(i,j,r)));
*
* --- share of intermediate demand cost for product i by sector j
*     on total production cost of sector j
*     (vom = value of total output = total cost)
*
*  thetai(i,j,r) $ vom(j,r)
*         = (vdfm(i,j,r)*(1+rtfd0(i,j,r)) + vifm(i,j,r)*(1+rtfi0(i,j,r))) / vom(j,r);
*
*
* --- share of total factor cost on total production cost of sector j
*     vfm=value of factor user by sector, rft0= related tax rate, benchmark
*
*  theta_f(j,r) $ vom(j,r) = sum(ff,vfm(ff,j,r)*(1+rtf0(ff,j,r))) / vom(j,r);
*
*
*
*ENERGY PRICE SHOCK: Calculation of the percentage increase
* Price_multiplier: 1+%Increase=1/p_techProgFac <=> p_techProgFac=1/(1+%Increase)
*
*